# Start with a base image containing Java Runtime

FROM openjdk:8-jdk-alpine

# Add maintainer info

LABEL maintainer="adesire@ebenyx.com@ebenyx.com"

# Add a volume pointing to /tmp

VOLUME /var/log

# Make port 8080 Available to the world outside this container

EXPOSE 9000

# The application jar file

ARG JAR_FILE=target/sgci_distribution_carte-1.0.jar
ARG RESOURCES=src/main/resources
ARG WEB_APP=src/main/webapp


# Add the application Jar to container

ADD ${JAR_FILE} /target/DistributionSgci.jar
COPY ${RESOURCES} /src/main/resources
COPY ${WEB_APP} /src/main/webapp


# Run the jar file

ENTRYPOINT ["java","-Djava-security.egd=file:/dev/./urandom","-jar","/target/DistributionSgci.jar"]

