/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebenyx.template.jsf_spring;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

/**
 *
 * @author ebenyx
 */
public class MyContext {
    String basePath;
    private static String templateGantt = null;
    
    
    public MyContext(){
        String aux = new File(".").getAbsolutePath();
        basePath = String.format("%ssrc/main/webapp",aux.substring(0,aux.length()-1));

    }

    public String getBasePath() {
        return basePath;
    }
    
    public String getRealPath(String path){
        return String.format("%s%s",basePath,path);
    }
    
    public String getRapportFolder(){
        return String.format("%s/resources/rapport", basePath);
    }
    
    public String getFileContentFolder(){
        return String.format("%s/resources/contenu_fichier", basePath);
    }
    
    public File getNewRapportFile(String fileName){
        return 
            new File(String.format("%s/resources/rapport/%s", basePath,fileName));
    }
    public Boolean move_file(File file, String destPath) throws IOException{
        Path temp=null;
        try{
            temp = Files.move(Paths.get(file.getAbsolutePath()),Paths.get(destPath),REPLACE_EXISTING);
        }
        catch(FileAlreadyExistsException exist){
            exist.printStackTrace();
        }
        return temp!=null;
    }
}
