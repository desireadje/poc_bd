/**
 *  (C) 2013-2014 Stephan Rauh http://www.beyondjava.net
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.ebenyx.template.jsf_spring.controller;

import com.ebenyx.template.jsf_spring.JsfUtil;
import com.ebenyx.template.jsf_spring.facade.AbstractFacade;
import com.ebenyx.template.jsf_spring.facade.ActeurFacade;
import com.ebenyx.template.jsf_spring.job.Job;
import com.ebenyx.template.jsf_spring.model.Acteur;
import com.ebenyx.template.jsf_spring.model.BaseEntity;
import com.ebenyx.template.jsf_spring.service.EmailService;
import java.util.HashMap;
import java.util.Stack;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.springframework.beans.factory.annotation.Autowired;

@ManagedBean
@SessionScoped
public class MainController extends AbstractController<BaseEntity> {

    @Autowired
    private transient ActeurFacade acteurFacade;

    @Autowired
    private transient EmailService emailService;

    @Autowired
    private transient Job job;

    private Stack<Object> pageStack;

    @Override
    protected void postConstruct() {
        pageStack = new Stack<>();
    }

    @Override
    public AbstractFacade getFacade() {
        throw new UnsupportedOperationException("Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public BaseEntity newEntity() {
        throw new UnsupportedOperationException("Not supported yet."); // To change body of generated methods, choose Tools | Templates.
    }

    public void loadActeur(String email, String token) {
        Acteur acteur = (Acteur) acteurFacade.builder()
                .where("email", email, "token", token).findOneEntity();
        set("acteur", acteur);
    }

    public String creerMdp() {
        Acteur acteur = (Acteur) get("acteur");
        acteur.setMotDePasse(ActeurFacade.getSecurePassword(getString("mdp")));
        acteur.setEnabled(true);
        acteur.updateToken();
        acteurFacade.edit(acteur);
        unsetField("mdp");

        return "/login.xhtml";
    }

    /*public void envoyerEmailChangementMdp(){
        _envoyerEmailChangementMdp(getString("email"));
    }*/

    /*public void _envoyerEmailChangementMdp(String email){
        acteurFacade.transaction(()->{
            Acteur acteur = (Acteur)acteurFacade.builder().where("email",email).findOneEntity();
            if(acteur == null){
                JsfUtil.msgErreur("Compte utilisateur introuvalble");
                return;
            }
            if(!acteur.isEnabled()){
                JsfUtil.msgErreur("Compte utilisateur inactif, veuillez finaliser la procedure d'inscription.");
                return;
            }
            
            acteur.updateToken();
            acteurFacade.edit(acteur);
            emailService.sendSimpleMessage(
                Mail
                    .createNew()
                    .setSubject("Reinitialiser mot de passe")
                    .setTo(acteur.getEmail())
                    .data("acteur",acteur)
                    .setTemplate("changer_mdp.html")
            ); 
            unsetField("email");
            JsfUtil.msgErreur("Une email de reinitialisation a été envoyé a l'adresse saisie");
        });
    }*/
    public void changerMdp() {
        Acteur acteur = getCurrentActeur();
        String mdp = acteur.getMotDePasse();
        String mdp0 = ActeurFacade.getSecurePassword(getString("mdp0"));
        String mdp1 = ActeurFacade.getSecurePassword(getString("mdp1"));
        if (mdp0.equals(mdp)) {
            acteur.setMotDePasse(mdp1);
            acteurFacade.edit(acteur);
            unsetField("mdp0", "mdp1");
        } else {
            JsfUtil.msgErreur("Echec d'authentification");
        }
    }

    public boolean canBack() {
        return !pageStack.empty();
    }

    public void back() {
        if (canBack()) {
            set("goBack", true);
            set("page", pageStack.pop());
            set("goBack", false);
        }
    }

    @Override
    protected void onPutField(String key, Object value) {
        if ("page".equals(key) && (!_boolean("goBack"))) {
            if (!value.equals(get(key))) {
                pageStack.push(get(key));
            }
            evenementFacade.navigation(value == null ? "Dashboard" : value.toString());
        }
    }

    public Stack<Object> getPageStack() {
        return pageStack;
    }

    @Override
    public void _onPageLoad() {
        String errorText = request.getParameter("error");
        if (errorText != null) {
            HashMap<String, String> map = new HashMap<>();
            map.put("-1", "Echec d'authentification !");
            map.put("-2", "Compte vérouillé !");
            JsfUtil.msgErreur(map.getOrDefault(errorText, "Autre erreur d'authentification !"));
        }
    }
}
