package com.ebenyx.template.jsf_spring.facade;

import com.ebenyx.template.jsf_spring.model.Service;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Component;

@Component
public class ServiceFacade extends AbstractFacade<Service>{
    
    @PersistenceContext 
    EntityManager em;

    public ServiceFacade() {
        super(Service.class);
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}


