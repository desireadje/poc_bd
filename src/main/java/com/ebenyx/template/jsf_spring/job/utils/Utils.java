/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebenyx.template.jsf_spring.job.utils;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {

    public static String formatAmount(double value) {
        DecimalFormat myFormatter = new DecimalFormat("###,###.###");
        return myFormatter.format(value);
    }

    /*
    public static String generateSerial(){
    int higher = 10000;
    int random = (int) (Math.random() * higher);
    Date date = new Date();
    String code = new StringBuilder(String.valueOf(random))
            .append(RandomStringUtils.randomAlphabetic(8)).append(date.getTime()).toString();

    return code;
  }
     */
    public static String generatePaymentNumber(long agentId) {
        long number = agentId + new Date().getTime();
        return String.valueOf(number);
    }

    public static String generatePaymentNumber(long agentId, Date date) {
        long number = agentId + date.getTime();
        return String.valueOf(number);
    }

    public static String generateInvoiceNumber(long agentId, Date date, int index) {
        long number = agentId + index + date.getTime();
        return String.valueOf(number);
    }

    public static long getReferenceIndex(String reference) {
        try {
            Pattern pattern = Pattern.compile(".*?-(.*?)-.*?");
            Matcher matcher = pattern.matcher(reference);
            if (matcher.find()) {
                return Long.parseLong(matcher.group(1));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return 0;
    }

    public static Date[] getExtremiteJour(Date jour) {
        Calendar date = Calendar.getInstance();
        date.setTime(jour);

        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, 0);
        date.set(Calendar.SECOND, 0);
        Date date1 = date.getTime();

        date.set(Calendar.HOUR_OF_DAY, 23);
        date.set(Calendar.MINUTE, 59);
        date.set(Calendar.SECOND, 59);
        Date date2 = date.getTime();
        return new Date[]{date1, date2};

    }
}
