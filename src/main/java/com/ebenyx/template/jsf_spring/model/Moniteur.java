/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebenyx.template.jsf_spring.model;

import com.ebenyx.template.jsf_spring.ws.WS;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.util.List;
import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(
        indexes = {
        @Index(columnList = "type")
    }
)
public class Moniteur extends BaseEntity {
    private int type;
    private String libelle;
    private String token;
    
    @Lob
    private String champJson;
    transient List<Champ> champList;

    public Moniteur() {
    }

    public Moniteur(WS.MoniteurJson moniteurJson){
        init(moniteurJson);
    }
    public void init(WS.MoniteurJson moniteurJson){
        System.out.println("-- moniteurJson = " + new Gson().toJson(moniteurJson));
        this.type = moniteurJson.getType();
        this.libelle = moniteurJson.getLibelle();
        this.champList = moniteurJson.getChampList();
        this.champJson = new Gson().toJson(moniteurJson.getChampList());
        this.token = UUID.randomUUID().toString();
        updateToken();
    }
    
    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getChampJson() {
        return champJson;
    }

    public void setChampJson(String champJson) {
        this.champJson = champJson;
    }

    public List<Champ> getChampList() {
        if(champList == null){
            this.champList = new Gson().fromJson(champJson,new TypeToken<List<Champ>>(){}.getType());
        }
        return champList;
    }

    public void setChampList(List<Champ> champList) {
        this.champList = champList;
    }

    public void updateToken() {
        this.token = UUID.randomUUID().toString();
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
    
    public static class Champ{
        final String libelle;
        final String description;
        final boolean requis;
        final String type;

        // Constructeur
        public Champ(String libelle, String description, boolean requis, String type) {
            this.libelle = libelle;
            this.description = description;
            this.requis = requis;
            this.type = type;
        }

        public String getLibelle() {
            return libelle;
        }

        public String getDescription() {
            return description;
        }

        public boolean isRequis() {
            return requis;
        }

        public String getType() {
            return type;
        }
        
        
    }
}