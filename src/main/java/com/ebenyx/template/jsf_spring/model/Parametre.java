package com.ebenyx.template.jsf_spring.model;

import com.ebenyx.template.jsf_spring.Main;
import java.util.Properties;
import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.javamail.JavaMailSenderImpl;

@Entity
@Table
public class Parametre extends BaseEntity {

    final static private Logger logger = LoggerFactory.getLogger(Main.class);

    //Merssagerie
    private String mailHost;
    private String mailUsername;
    private String mailSender;
    private String mailPassword;
    private int mailPort;
    private String mailProtocol;
    private boolean mailSmtpAuth;
    private String mailSmtpAuthMechanisms;
    private String mailSmtpAuthNtlmDomain;
    private boolean mailSmtpStarttlsEnable;
    private boolean mailPropertiesMailDebugAuth;
    private String mailTest;

    //design confguration
    private String menuMode = "layout-static";
    private String theme = "blue-accent";
    private String layout = "blue";
    private boolean darkMenu = true;
    private String profileMode = "popup";
    private boolean groupedMenu = true;
    private boolean darkLogo;
    private boolean orientationRTL;

    private String httpUrl;
    
    private String wsToken;
    private int delaisDeRafraichissement;
    private int delaisDeRafraichissementMoniteur;
    
    public Parametre() {
    }

    public String getMailHost() {
        return mailHost;
    }

    public void setMailHost(String mailHost) {
        this.mailHost = mailHost;
    }

    public String getMailUsername() {
        return mailUsername;
    }

    public void setMailUsername(String mailUsername) {
        this.mailUsername = mailUsername;
    }

    public String getMailSender() {
        return mailSender;
    }

    public void setMailSender(String mailSender) {
        this.mailSender = mailSender;
    }

    public String getMailPassword() {
        return mailPassword;
    }

    public void setMailPassword(String mailPassword) {
        this.mailPassword = mailPassword;
    }

    public int getMailPort() {
        return mailPort;
    }

    public void setMailPort(int mailPort) {
        this.mailPort = mailPort;
    }

    public String getMailProtocol() {
        return mailProtocol;
    }

    public void setMailProtocol(String mailProtocol) {
        this.mailProtocol = mailProtocol;
    }

    public boolean getMailSmtpAuth() {
        return mailSmtpAuth;
    }

    public void setMailSmtpAuth(boolean mailSmtpAuth) {
        this.mailSmtpAuth = mailSmtpAuth;
    }

    public String getMailSmtpAuthMechanisms() {
        return mailSmtpAuthMechanisms;
    }

    public void setMailSmtpAuthMechanisms(String mailSmtpAuthMechanisms) {
        this.mailSmtpAuthMechanisms = mailSmtpAuthMechanisms;
    }

    public String getMailSmtpAuthNtlmDomain() {
        return mailSmtpAuthNtlmDomain;
    }

    public void setMailSmtpAuthNtlmDomain(String mailSmtpAuthNtlmDomain) {
        this.mailSmtpAuthNtlmDomain = mailSmtpAuthNtlmDomain;
    }

    public boolean getMailSmtpStarttlsEnable() {
        return mailSmtpStarttlsEnable;
    }

    public void setMailSmtpStarttlsEnable(boolean mailSmtpStarttlsEnable) {
        this.mailSmtpStarttlsEnable = mailSmtpStarttlsEnable;
    }

    public boolean getMailPropertiesMailDebugAuth() {
        return mailPropertiesMailDebugAuth;
    }

    public void setMailPropertiesMailDebugAuth(boolean mailPropertiesMailDebugAuth) {
        this.mailPropertiesMailDebugAuth = mailPropertiesMailDebugAuth;
    }

    public String getMailTest() {
        return mailTest;
    }

    public void setMailTest(String mailTest) {
        this.mailTest = mailTest;
    }

    public void setup(JavaMailSenderImpl mailSender) {
        try {
            mailSender.setHost(this.getMailHost());
            mailSender.setPort(this.getMailPort());
            mailSender.setUsername(this.getMailUsername());
            mailSender.setPassword(this.getMailPassword());
            mailSender.setProtocol(this.getMailProtocol());
            Properties props = mailSender.getJavaMailProperties();

            props.put("mail.smtp.auth", this.getMailSmtpAuth());
            if (!this.getMailSmtpAuthMechanisms().isEmpty()) {
                props.put("mail.smtp.auth.mechanisms", this.getMailSmtpAuthMechanisms());
            }

            if (!this.getMailSmtpAuthNtlmDomain().isEmpty()) {
                props.put("mail.smtp.auth.ntlm.domain", this.getMailSmtpAuthNtlmDomain());
            }
            props.put("mail.username", this.getMailUsername());
            props.put("mail.password", this.getMailPassword());
            props.put("mail.smtp.host", this.getMailHost());
            props.put("mail.smtp.port", this.getMailPort());
            // props.put("mail.smtp.starttls.enable", this.getMailSmtpStarttlsEnable());
        } catch (Exception ex) {
            logger.error("", ex);
        }
    }

    public String getMenuMode() {
        return menuMode;
    }

    public void setMenuMode(String menuMode) {
        this.menuMode = menuMode;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getLayout() {
        return layout;
    }

    public void setLayout(String layout) {
        this.layout = layout;
    }

    public boolean getDarkMenu() {
        return darkMenu;
    }

    public void setDarkMenu(boolean darkMenu) {
        this.darkMenu = darkMenu;
    }

    public String getProfileMode() {
        return profileMode;
    }

    public void setProfileMode(String profileMode) {
        this.profileMode = profileMode;
    }

    public boolean getGroupedMenu() {
        return groupedMenu;
    }

    public void setGroupedMenu(boolean groupedMenu) {
        this.groupedMenu = groupedMenu;
    }

    public boolean getDarkLogo() {
        return darkLogo;
    }

    public void setDarkLogo(boolean darkLogo) {
        this.darkLogo = darkLogo;
    }

    public boolean getOrientationRTL() {
        return orientationRTL;
    }

    public void setOrientationRTL(boolean orientationRTL) {
        this.orientationRTL = orientationRTL;
    }

    public void defaultTheme() {
        this.menuMode = "layout-static";
        this.theme = "blue-accent";
        this.layout = "blue";
        this.darkMenu = true;
        this.profileMode = "popup";
        this.groupedMenu = true;
        this.darkLogo = false;
        this.orientationRTL = false;
    }

    public String getHttpUrl() {
        return httpUrl;
    }

    public void setHttpUrl(String httpUrl) {
        this.httpUrl = httpUrl;
    }

    public int getDelaisDeRafraichissement() {
        return delaisDeRafraichissement;
    }

    public void setDelaisDeRafraichissement(int delaisDeRafraichissement) {
        this.delaisDeRafraichissement = delaisDeRafraichissement;
    }

    public String getWsToken() {
        return wsToken;
    }

    public void setWsToken(String wsToken) {
        this.wsToken = wsToken;
    }
    public void refreshToken(){
        this.wsToken = UUID.randomUUID().toString();
    }

    public int getDelaisDeRafraichissementMoniteur() {
        return delaisDeRafraichissementMoniteur;
    }

    public void setDelaisDeRafraichissementMoniteur(int delaisDeRafraichissementMoniteur) {
        this.delaisDeRafraichissementMoniteur = delaisDeRafraichissementMoniteur;
    }
}
