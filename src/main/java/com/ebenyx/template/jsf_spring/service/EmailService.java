/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebenyx.template.jsf_spring.service;

import com.ebenyx.template.jsf_spring.MyContext;
import com.ebenyx.template.jsf_spring.facade.ParametreFacade;
import com.ebenyx.template.jsf_spring.job.mail.Mail;
import com.ebenyx.template.jsf_spring.model.Parametre;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import java.io.File;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Map;
import org.apache.commons.io.FileUtils;
import org.springframework.scheduling.annotation.Async;

@Service
public class EmailService {

    @Autowired
    private MyContext myContext;

    @Autowired
    private JavaMailSender emailSender;

    @Autowired
    private Configuration freemarkerConfig;

    @Autowired
    private ParametreFacade parametreFacade;

    @Async
    public void sendSimpleMessage(Mail mail) throws MessagingException, IOException, TemplateException {
        _sendSimpleMessage(mail);
    }

    public void _sendSimpleMessage(Mail mail) throws MessagingException, IOException, TemplateException {
        MimeMessage message = emailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                StandardCharsets.UTF_8.name());

        String header = Base64.getEncoder().encodeToString(FileUtils.readFileToByteArray(
                new File(String.format("%s/resources/images/header.png", myContext.getBasePath()))));
        String logo = Base64.getEncoder().encodeToString(FileUtils.readFileToByteArray(
                new File(String.format("%s/resources/images/ebt_logo.png", myContext.getBasePath()))));

        Parametre p = parametreFacade.get();
        Template t = freemarkerConfig.getTemplate(mail.getTemplate());
        
        mail.data("logo", logo);
        mail.data("header", header);
        mail.data("httpUrl", p.getHttpUrl());
        
        String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, mail.getModel());

        if (mail.getCC() != null && (!mail.getCC().trim().isEmpty())) {
            helper.setCc(mail.getCC().split(";"));
        }
        
        helper.setFrom(p.getMailSender());
        helper.setTo(mail.getTo());
        helper.setText(html, true);
        helper.setSubject(" MONITORING - " + mail.getSubject());
        for (Map.Entry<String, File> entry : mail.getAttachement().entrySet()) {
            helper.addAttachment(entry.getKey(), entry.getValue());
        }
        System.out.println("envoie de email ... '" + mail.getTo() + "'");
        
        emailSender.send(message);
    }
}
