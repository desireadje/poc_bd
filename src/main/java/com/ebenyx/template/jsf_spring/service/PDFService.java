/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebenyx.template.jsf_spring.service;

import com.ebenyx.template.jsf_spring.MyContext;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

@Service
public class PDFService {
    public static interface Listener{
        void _initRender();
        void _onEndPage(PdfWriter writer, Document document, int count);
    }
    private static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    private static final SimpleDateFormat ddMMMMyyyy = new SimpleDateFormat("dd MMMM yyyy");
    private static final SimpleDateFormat ddMMMyyyy = new SimpleDateFormat("dd MMM yyyy");
    
    @Autowired
    private Configuration freemarkerConfig;
    @Autowired
    private MyContext myContext;
    
    public File generatePdf(
            String fileName,
            String template,
            HashMap<String,Object> model, 
            String directory ) throws TemplateException, IOException, DocumentException {
        return generatePdf(null, fileName, template, model, directory,null);
    }
    
    public File generatePdf(
            String fileName,
            String template,
            HashMap<String,Object> model,
            String directory,
            final Listener listener) throws TemplateException, IOException, DocumentException {
        return generatePdf(null, fileName, template, model, directory,listener);
    }
    
    public File generatePdf(
            Document document,
            String fileName,
            String template,
            String directory,
            HashMap<String,Object> model) throws TemplateException, IOException, DocumentException {
        return generatePdf(document, fileName, template, model, directory, null);
    }
    public File generatePdf(
            Document document,
            String fileName,
            String template,
            HashMap<String,Object> model,
            String directory,
            final Listener listener) throws TemplateException, IOException, DocumentException {
        Template t = freemarkerConfig.getTemplate(template);
        Date date =new Date();
        model.put("today",sdf.format(date));
        model.put("ddMMMMyyyy",ddMMMMyyyy.format(date));
        model.put("ddMMMyyyy",ddMMMyyyy.format(date));
        String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, model);
        
        File dir = new File (directory);
        File file = new File(directory+"/"+fileName);    
        FileOutputStream pdf = new FileOutputStream(file);
        if(document == null){
            document = new Document();
        }
        PdfWriter writer = PdfWriter.getInstance(document, pdf);
        document.open();
        
        if(listener != null){
            writer.setPageEvent(new PdfPageEventHelper(){
                int COUNT = 1;
                private PdfPageEventHelper init(){
                    listener._initRender();
                    return this;
                }
                @Override
				public void onEndPage(PdfWriter writer, Document document) {
                    listener._onEndPage(writer,document, COUNT);
                    COUNT++;
                }
            }.init());
        }
        System.out.println(html.toString());
        InputStream is = new ByteArrayInputStream(html.toString().getBytes());
        
        XMLWorkerHelper.getInstance().parseXHtml(writer, document, is);
        document.close();
        pdf.close();
        System.out.println("pdf : "+file.getAbsolutePath());
        return file;
    }
    

}