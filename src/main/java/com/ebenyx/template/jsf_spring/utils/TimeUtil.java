/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ebenyx.template.jsf_spring.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author ftako
 */
public class TimeUtil {

    private static final String PATTERN = "dd/MM/yyyy";

    public static boolean compareDate(Date startDate, Date endDate, Date now) {
        Calendar date = Calendar.getInstance();
        Calendar cStartDate = Calendar.getInstance();
        Calendar cEndDate = Calendar.getInstance();
        date.setTime(now);
        cStartDate.setTime(startDate);
        cEndDate.setTime(endDate);

        return date.after(cStartDate) && date.before(cEndDate);
    }

    public static long getDifferenceDays(Date d1, Date d2) {
        long diff = d2.getTime() - d1.getTime();
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    }

    public static Date parseDate(String value) throws ParseException {
        DateFormat formatter = new SimpleDateFormat(PATTERN);
        return formatter.parse(value);
    }

    public static Date parseDate(String value, String pattern) throws ParseException {
        DateFormat formatter = new SimpleDateFormat(pattern);
        return formatter.parse(value);
    }

    public static String formatDate(Date value) throws ParseException {
        DateFormat formatter = new SimpleDateFormat(PATTERN);
        return formatter.format(value);
    }

    public static String formatDate(Date date, String pattern) throws ParseException {
        DateFormat formatter = new SimpleDateFormat(pattern);
        return formatter.format(date);
    }

    public static Date getStartAndEndOfDate(Date date) throws ParseException {
        String dateString = formatDate(date);
        return parseDate(dateString);
    }

    public static List<Date> getFirstDateAndLastDateOfCurrentMonth(Calendar cal) {
        List<Date> _dates = new ArrayList<>();
        cal.add(Calendar.MONTH, 0);
        cal.set(Calendar.DATE, 1);
        _dates.add(cal.getTime());
        cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE));
        _dates.add(cal.getTime());
        return _dates;
    }

    public static List<Date> getLastWeekStartDateAndNextWeekEndDateOfCurrentMonth(Calendar cal) {
        List<Date> _dates = new ArrayList<>();

        // cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        // cal.add(Calendar.WEEK_OF_YEAR, -1);
        cal.add(Calendar.DATE, -7);

        _dates.add(cal.getTime());

        System.out.println("FIRST DATE --> " + cal.getTime().toString());

        cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 6);
        // cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        // cal.add(Calendar.WEEK_OF_YEAR, 1);
        // cal.add(Calendar.DATE, 6);

        _dates.add(cal.getTime());

        System.out.println("END DATE --> " + cal.getTime().toString());
        return _dates;
    }

    public static void test() {
        Calendar c = Calendar.getInstance(Locale.FRANCE);
        // Set the calendar to monday of the current week
        c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        // Print dates of the current week starting on Monday
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        String startDate = "", endDate = "";

        /*
            * startDate = df.format(c.getTime()); c.add(Calendar.DATE, 6); endDate =
            * df.format(c.getTime());
            * 
            * System.out.println("Current Week Start Date = " + startDate);
            * System.out.println("Current Week End Date = " + endDate);
         */
        c.add(Calendar.WEEK_OF_YEAR, -1);
        startDate = df.format(c.getTime());
        c.add(Calendar.DATE, 6);
        endDate = df.format(c.getTime());

        System.out.println("Last Week Start Date = " + startDate);
        System.out.println("Last Week End Date = " + endDate);

        // c = Calendar.getInstance();
        // c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        c.add(Calendar.WEEK_OF_YEAR, 1);
        startDate = df.format(c.getTime());
        c.add(Calendar.DATE, 6);
        endDate = df.format(c.getTime());

        System.out.println("Next Week Start Date = " + startDate);
        System.out.println("Next Week End Date = " + endDate);
    }

    public static Calendar getCalendarByMonth(Calendar calendar, int month) {
        switch (month) {
            case 0:
                calendar.set(Calendar.MONTH, Calendar.JANUARY);
                break;
            case 1:
                calendar.set(Calendar.MONTH, Calendar.FEBRUARY);
                break;
            case 2:
                calendar.set(Calendar.MONTH, Calendar.MARCH);
                break;
            case 3:
                calendar.set(Calendar.MONTH, Calendar.APRIL);
                break;
            case 4:
                calendar.set(Calendar.MONTH, Calendar.MAY);
                break;
            case 5:
                calendar.set(Calendar.MONTH, Calendar.JUNE);
                break;
            case 6:
                calendar.set(Calendar.MONTH, Calendar.JULY);
                break;
            case 7:
                calendar.set(Calendar.MONTH, Calendar.AUGUST);
                break;
            case 8:
                calendar.set(Calendar.MONTH, Calendar.SEPTEMBER);
                break;
            case 9:
                calendar.set(Calendar.MONTH, Calendar.OCTOBER);
                break;
            case 10:
                calendar.set(Calendar.MONTH, Calendar.NOVEMBER);
                break;
            case 11:
                calendar.set(Calendar.MONTH, Calendar.DECEMBER);
                break;
            default:
                break;
        }
        return calendar;
    }

    public static Date getTomorrowDate(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, 1);
        return c.getTime();
    }

    public static Long dateSecondeDifference(String date1, String date2, String pattern) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);

        Date d1 = sdf.parse(date1);
        Date d2 = sdf.parse(date2);

        Long diffInMillis = d2.getTime() - d1.getTime();
        Long dateDiffInDays = TimeUnit.DAYS.convert(diffInMillis, TimeUnit.MILLISECONDS);

        Long dateDiffInHours = TimeUnit.HOURS.convert(diffInMillis - (dateDiffInDays * 24 * 60 * 60 * 1000),
                TimeUnit.MILLISECONDS);

        Long dateDiffInMinutes = TimeUnit.MINUTES.convert(
                diffInMillis - (dateDiffInDays * 24 * 60 * 60 * 1000) - (dateDiffInHours * 60 * 60 * 1000),
                TimeUnit.MILLISECONDS);

        Long dateDiffInSeconds = TimeUnit.SECONDS.convert(diffInMillis - (dateDiffInDays * 24 * 60 * 60 * 1000)
                - (dateDiffInHours * 60 * 60 * 1000) - (dateDiffInMinutes * 60 * 1000), TimeUnit.MILLISECONDS);

        //System.out.println(dateDiffInDays + " day(s) " + dateDiffInHours + " Hour(s) " + dateDiffInMinutes
        // 	+ " Minute(s) " + dateDiffInSeconds + " Second(s)");
        return dateDiffInSeconds;

    }

    public static Long differenceEnSeconde(Date dateFin, Date dataDebut) {
        // Date startDate = sentDate;// Set start date
        // Date endDate = new Date(); // Set end date

        Long duration = dateFin.getTime() - dataDebut.getTime();

        Long diffInSeconds = TimeUnit.MILLISECONDS.toSeconds(duration);
        // Long diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(duration);
        // Long diffInHours = TimeUnit.MILLISECONDS.toHours(duration);
        // Long diffInDays = TimeUnit.MILLISECONDS.toDays(duration);

        return diffInSeconds;
    }

    // Augmenter une date jour par jour
    public static Date addDays(Date date, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days); // minus number would decrement the days
        return cal.getTime();
    }

    // Augmenter une date jour par jour
    public static Date addMinutes(Date date, int minute) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MINUTE, minute); // minus number would decrement the minute
        return cal.getTime();
    }

    public Date addMonth(Date date, int minute) {
        Date now = date;
        Calendar myCal = Calendar.getInstance();
        myCal.setTime(now);
        myCal.add(Calendar.MONTH, +minute);
        now = myCal.getTime();
        System.out.println("===========> " + now);
        return now;
    }

}
