package ${facadePackage};

import ${modelPackage}.${modelClass.simpleName};
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Component;

@Component
public class ${modelClass.simpleName}Facade extends AbstractFacade<${modelClass.simpleName}>{
    
    @PersistenceContext 
    EntityManager em;

    public ${modelClass.simpleName}Facade() {
        super(${modelClass.simpleName}.class);
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
}


